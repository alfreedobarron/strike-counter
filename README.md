# Strike Counter

Aplicación para el conteo de _strikes_ de los integrantes de GISAA, usando [Silex](http://silex.sensiolabs.org/), [Twig]() y PDO, funcionando sobre [Heroku](http://heroku.com).


## Funcionamiento Básico

El _conteo de strikes_ consiste en registrar un _strike_, por cada ocasión que un integrante de GISAA comete un error del tipo:

- Hablar mal de uno de los docentes a __espaldas de ellos__, cuando estan presentes.
- Cambiar una o más líneas de código que afecten de manera parcial o completa las aplicaciones en _production_
- Hacer un comentario _retador_ hacia alguno de los integrantes, tal que amerite registrar su número de control.
- Usar _Wondows_ en presencia del Dr, y que este lo note.
- Otros

La aplicación deberá permitir, después de el respectivo inicio de sesión, dar de alta un _strike_, indicando por lo menos:

- Fecha y hora
- Motivo
- Quién recibe
- Quién otorga

### Reglas

- Nota mala = 1 strike
- 3 strikes = 1 Out

## Motivación

Esta aplicación sirve como ejercicio de colaboración en el desarrollo de proyectos para [GISAA](http://www.adaptivez.com.mx/gisaa), usando Git y Github como medios, para posteriormente ser utilizado en proyectos de mayor escala.

## Proceso Básico de Colaboración

1. Crear cuenta en Github
2. Generar y agregar llaves SSH
3. Clonar proyecto en local
4. Seleccionar un módulo a colaborar
5. Realizar cambios y hacer _commits_
6. Descargar cambios
7. Resolver conflictos de código y subir cambios
8. Repetir pasos 4, 5, 6 y 7 tantas veces como sea necesario hasta terminar la aplicación

### Comandos Básicos

```sh
$ git clone git@github.com:j2deme/strike-counter.git
$
$ git add archivo
$
$ git commit -m "Mensaje"
$
$ git pull origin master
$
$ git push origin master
```

Para pruebas en local solo es necesario tener la copia de la base de datos y cambiar los respectivos valores de conexión.

[Yo](http://github.com/j2deme) me encargare de hacer el _deploy_ en Heroku, para que los cambios se vayan actualizando en lo que podríamos llamar __servidor central__.

### Documentación

Para mayor información sobre lo que estamos utilizando revisar los siguientes enlaces:

- [Silex](http://silex.sensiolabs.org/documentation)
- [Twig](http://twig.sensiolabs.org/doc/templates.html)
- [PDO](http://code.tutsplus.com/tutorials/why-you-should-be-using-phps-pdo-for-database-access--net-12059)
- [PHP](http://php.net)
- [Heroku](http://heroku.com)
- [Google](http://google.com)

# Integrantes

- Jaime Delgado
- Alfredo Barrón
- Jorge de Jesus Espinoza Martinez
- Gina Huerta
- Carolina
- _Su nombre aquí_
- ¿Junto a nuestro nombre vamos a indicar el módulo a realizar? ¿Cuáles son los módulos? No entiendo. u_u