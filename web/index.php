<?php
require('../vendor/autoload.php');
require_once __DIR__.'/config.php';

$app->get('/', function() use($app) {
  return $app['twig']->render('index.twig');
})->bind('home');

$app->post('/login', function() use($app) {
  return true;
})->bind('login');

$app->run();

?>
