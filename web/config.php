<?php
// CONFIG
define('SYS_TITLE', 'Strike Counter');
define('SYS_NAME', 'Strike Counter');
define('SYS_DEBUG', true);

// FOLDERS
define('ASSETS_DIR', '/assets/');
define('CONTROLLERS_DIR', 'controllers/');
define('VIEWS_DIR', 'views/');
define('MODELS_DIR', 'models/');

// DATABASE
define('DB_HOST', '127.0.0.1');
define('DB_PORT', '');
define('DB_NAME', 'blog');
define('DB_USER', 'ht');//Su usuario de postgres
define('DB_PASS', '');//Su contraseña de postgres

session_cache_limiter(false);
date_default_timezone_set('America/Mexico_City');

$app = new Silex\Application();
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\SwiftmailerServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(),array(
  'twig.path' => VIEWS_DIR,
  'twig.options' => array(
    'auto_reload' => true,
    'debug' => true
  )
));

$app['title'] = SYS_NAME;

$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {
  $twig->addFunction(new \Twig_SimpleFunction('asset', function ($asset) use($app) {
    $ext = pathinfo($asset, PATHINFO_EXTENSION);
    $base = $app['request']->getBaseUrl();
    switch ($ext) {
      case 'css':
        $dir = $base.ASSETS_DIR.'css';
        break;
      case 'js':
        $dir = $base.ASSETS_DIR.'js';
        break;
      case 'jpg':
      case 'png':
      case 'gif':
        $dir = $base.ASSETS_DIR.'img';
        break;
      default:
        $dir = $base;
        break;
    }
    return sprintf("$dir/%s", ltrim($asset, '/'));
  }));
  $twig->addFunction(new Twig_SimpleFunction('active', function ($name) use ($app) {
    if ($name === $app['request']->get('_route')) {
      if (isset($app['active_link.snippet'])) {
        return $app['active_link.snippet'];
      } else {
        return ' class="active"';
      }
    }
    return '';
    },
    array(
      'is_safe' => array('html')
    )
  ));

  return $twig;
}));

if(getenv('DATABASE_URL') != false){
  $dbopts = parse_url(getenv('DATABASE_URL'));
  $app->register(new Herrera\Pdo\PdoServiceProvider(),
    array(
      'pdo.dsn' => 'pgsql:dbname='.ltrim($dbopts["path"],'/').';host='.$dbopts["host"],
      'pdo.port' => $dbopts["port"],
      'pdo.username' => $dbopts["user"],
      'pdo.password' => $dbopts["pass"]
    )
  );
} else {
  $app->register(new Herrera\Pdo\PdoServiceProvider(),
    array(
      'pdo.dsn'      => "pgsql:host=".DB_HOST.";dbname=".DB_NAME,
      'pdo.username' => DB_USER,
      'pdo.password' => DB_PASS,
      'pdo.options'  => array(
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES   => false,
        PDO::ATTR_PERSISTENT         => true,
      )
    )
  );
}

$app['debug'] = SYS_DEBUG;
